// External dependencies
import React from "react";

/**
 * class contains all the partners of the company.
 * it contains images of the partners.
 * data of partners is rendered from  app.js using props
 */
class Partner extends React.Component {
  render() {
    return (
      <>
        <section id="partners" className="brand-logos">
          <h1 className="sec-heading">Our Partners</h1>
          <div>
            {/* map function is used for looping through the data */}
            {this.props.shorts.map((data) => {
              // Part class is called in the loop
              return <Part src={data.src} alt={data.alt} title={data.title} />;
            })}
          </div>
        </section>
      </>
    );
  }
}

/**
 * class renders images of partners using props
 */
class Part extends React.Component {
  render() {
    return (
      <a>
        <img
          src={this.props.src}
          alt={this.props.alt}
          title={this.props.title}
        />
      </a>
    );
  }
}

//exporting partners class
export default Partner;
