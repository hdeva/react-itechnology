// External dependencies
import React from "react";

// Internal dependencies
import "./main.css";

import Navbar from "./navbar";
import Intro from "./intro";
import Delivery from "./delivery";
import Service from "./service";
import Success from "./success";
import Revenue from "./revenue";
import Highlights from "./highlights";
import Partner from "./partners";
import Toplist from "./toplist";
import Footer from "./footer";


/**
 * Class component renders all the components of the webpage.
 * Data from app.js is rendered using props
 */
class Main extends React.Component {
  render() {
    return (
      <>
        <Navbar />
        <Intro />
        <Delivery />
        <Service shortcuts={this.props.shortcuts} />
        <Success />
        <Revenue imgs={this.props.imgs} />
        <Highlights />
        <Partner shorts={this.props.shorts} />
        <Toplist tops={this.props.tops} />
        <Footer data={this.props.data} />
      </>
    );
  }
}

//exporting main class
export default Main;
