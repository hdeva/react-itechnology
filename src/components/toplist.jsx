// External dependency
import React from "react";

/**
 * class contains recognised as top mobile development company and
 * top companies list.data of toplists is rendered from  app.js using props
 */
class Toplist extends React.Component {
  render() {
    return (
      <>
        <section id="topList" className="brand-logos">
          <h1 className="sec-heading">
            Recognition as Top Mobile Development Company
          </h1>
          <div>
            {/* map function is used for looping through the data */}
            {this.props.tops.map((data) => {
              // Lists class is called in the loop
              return (
                <Lists
                  src={data.src}
                  alt={data.alt}
                  title={data.title}
                  text={data.text}
                />
              );
            })}
          </div>
        </section>
      </>
    );
  }
}
/**
 * Class renders images of the companies using props
 */
class Lists extends React.Component {
  render() {
    return (
      <a>
        <img
          src={this.props.src}
          alt={this.props.alt}
          title={this.props.title}
        />
        <span>{this.props.text}</span>
      </a>
    );
  }
}

//exporting toplist class
export default Toplist;
