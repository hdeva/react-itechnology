// External dependencies
import React from "react";

/**
 * class contains images of best companies with good revenue.
 * images of companies is rendered from  app.js using props
 */
class Revenue extends React.Component {
  render() {
    return (
      <>
        <section id="revenue" className="brand-logos">
          <h1 className="sec-heading">
            We Drive Growth & Revenue for the Best Companies
          </h1>
          <div>
            {/* map function is used for looping through the data */}
            {this.props.imgs.map((data) => {
              // Reven class is called in the loop
              return <Reven src={data.src} alt={data.alt} title={data.title} />;
            })}
          </div>
        </section>
      </>
    );
  }
}

/**
 * class renders images of companies with best revenue using props
 */
class Reven extends React.Component {
  render() {
    return (
      <a>
        <img
          src={this.props.src}
          alt={this.props.alt}
          title={this.props.title}
        />
      </a>
    );
  }
}

//exporting revenue class
export default Revenue;
