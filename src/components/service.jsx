//External dependencies
import React from "react";

/**
 * class contains list of Services of the company with images.
 * data of services is rendered from  app.js using props
 */
class Service extends React.Component {
  render() {
    return (
      <>
        <section id="services">
          <h1 className="sec-heading">Our Services</h1>
          <ul>
            {/* map function is used for looping through the data */}
            {this.props.shortcuts.map((data) => {
              // Serve class is called in the loop
              return <Serve icon={data.icon} name={data.name} />;
            })}
          </ul>

          <div id="service-footer">
            <a href="" className="brand-btn">
              View All Service
            </a>
          </div>
        </section>
      </>
    );
  }
}
/**
 * class renders images and name of the services offered by company using props
 */
class Serve extends React.Component {
  render() {
    return (
      <li>
        <div>
          <a href="">
            <i className={this.props.icon}></i>
            <span>{this.props.name}</span>
          </a>
        </div>
      </li>
    );
  }
}

//exporting service class
export default Service;
