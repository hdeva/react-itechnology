//External dependecies
import React from "react";

//Internal dependecies
import Main from "./components/main";

class App extends React.Component {
  // data for services component
  shortcuts = [
    { icon: "fas fa-laptop", name: "Strategy and Consultant" },
    { icon: "fas fa-users", name: "User Experience Design" },
    { icon: "fas fa-mobile-alt", name: "Mobile App Development" },
    { icon: "fab fa-chrome", name: "Web App Development" },
    { icon: "fas fa-ribbon", name: "Quality Analysis and Testing" },
    { icon: "fas fa-ticket-alt", name: "Application Management & Support" },
  ];
  // data for revenue component
  imgs = [
    {
      src: "https://www.dropbox.com/s/lmvtthec9yn0ti6/Allianz.png?raw=1",
      alt: "Allianz",
      title: "Work with Allianz",
    },
    {
      src: "https://www.dropbox.com/s/kotgq2u4qr34i2u/audi.jpg?raw=1",
      alt: "Audi",
      title: "Work with Audi",
    },
    {
      src: "https://www.dropbox.com/s/t5dapt3lkz7rdhe/BMW.png?raw=1",
      alt: "BMW",
      title: "Work with BMW",
    },
    {
      src: "https://www.dropbox.com/s/ocqbsbgj590ztyy/ESPN.png?raw=1",
      alt: "ESPN",
      title: "Work with ESPN",
    },
    {
      src: "https://www.dropbox.com/s/2maaqxijcmbaqxg/LG.png?raw=1",
      alt: "LG",
      title: "Work with LG",
    },
    {
      src: "https://www.dropbox.com/s/yn3gj203hrdjfu7/Logo_NIKE.png?raw=1",
      alt: "Nike",
      title: "Work with Nike",
    },
    {
      src: "https://www.dropbox.com/s/gfxa6exv7h1ro6q/Suzuki_logo.png?raw=1",
      alt: "Suzuki",
      title: "Work with Suzuki",
    },
    {
      src: "https://www.dropbox.com/s/b7vwmjf6e0owybv/Visa.svg?raw=1",
      alt: "Visa",
      title: "Work with Visa",
    },
  ];

  // data for partner component
  shorts = [
    {
      src: "https://www.dropbox.com/s/mk5ca04seizpf8l/aws.svg?raw=1",
      alt: "Work with AWS",
      title: "Our Work",
    },
    {
      src: "https://www.dropbox.com/s/r9utt5nj9k9m1t8/Dell.png?raw=1",
      alt: "Dell",
      title: "Work with Dell",
    },
    {
      src: "https://www.dropbox.com/s/umw9g0zgm1ecfvn/Intel.png?raw=1",
      alt: "intel",
      title: "Work with intell",
    },
    {
      src: "https://www.dropbox.com/s/x0hrha2dosey99z/ibm.png?raw=1",
      alt: "IBM",
      title: "Work with IBM",
    },
    {
      src: "https://www.dropbox.com/s/ekzu1wcki6jziay/Microsoft.svg?raw=1",
      alt: "Microsoft",
      title: "Work with Microsoft",
    },
    {
      src: "https://www.dropbox.com/s/lvl5cp14i3v0wgi/Nasscom.png?raw=1",
      alt: "Nasscom",
      title: "Work with Nasscom",
    },
    {
      src: "https://www.dropbox.com/s/h66k9jaaknxaum4/Samsung.png?raw=1",
      alt: "Samsung",
      title: "Work with Samsung",
    },
    {
      src: "https://www.dropbox.com/s/86cbtf78khj0q9z/Nvidia.png?raw=1",
      alt: "Nvidia",
      title: "Work with Nvidia",
    },
  ];

  // data for toplists component
  tops = [
    {
      src: "https://www.dropbox.com/s/19czj59oq0orbfa/tm.png?raw=1",
      alt: "Top 10 MobleApp Development Companies",
      title: "Top 10 MobleApp Development Companies",
      text: "Recognised Among Top 10 MobleApp Development Companies",
    },
    {
      src: "https://www.dropbox.com/s/130734rofy1f261/tata.png?raw=1",
      alt: "Top 10 MobleApp Development Companies",
      title: "Top 10 MobleApp Development Companies",
      text: "Recognised Among Top 10 MobleApp Development Companies",
    },
    {
      src: "https://www.dropbox.com/s/k17kwv9hiu9w98d/Infosys_logo.png?raw=1",
      alt: "Top 10 MobleApp Development Companies",
      title: "Top 10 MobleApp Development Companies",
      text: "Recognised Among Top 10 MobleApp Development Companies",
    },
    {
      src: "https://www.dropbox.com/s/mm4cnforc4pvwac/Wipro_Logo.png?raw=1",
      alt: "Top 10 MobleApp Development Companies",
      title: "Top 10 MobleApp Development Companies",
      text: "Recognised Among Top 10 MobleApp Development Companies",
    },
    {
      src: "https://www.dropbox.com/s/n4scpig8b3tfqkq/Amazon_logo.svg?raw=1",
      alt: "Top 10 MobleApp Development Companies",
      title: "Top 10 MobleApp Development Companies",
      text: "Recognised Among Top 10 MobleApp Development Companies",
    },
  ];
  // data for footer component
  data = {
    Solution: ["Healthcare", "Sports", "ECommerce", "Construction", "Club"],
    Industry: [
      "Interprise App Development",
      "Android App Development",
      "ios App Development",
    ],
    "Quick Links": ["Reviews", "Terms & Condition", "Disclaimer", "Site Map"],
  };
  //rendering main class 
  render() {
    return (
      <Main
        shortcuts={this.shortcuts}
        imgs={this.imgs}
        shorts={this.shorts}
        data={this.data}
        tops={this.tops}
      />
    );
  }
}

//exporting app class
export default App;
